import { useState } from 'react';

const levels = {
	Beginner: { size: [9, 9], mines: 9 },
	Intermediate: { size: [16, 16], mines: 40 },
	Expert: { size: [30, 16], mines: 99 }
};

const App = () => {

	type Cell = { pos: number[], mine?: true, open?: true };
	type Cells = Cell[];

	const range = <T,>(length: number, fn: (i: number) => T) =>
		[...Array(length).keys()].map(fn);
	const level = levels.Beginner;
	const { mines } = level;
	const createBoard = ([x, y]: number[]): Cells[] =>
		range(y, y => range(x, x => ({ pos: [x, y] })));
	const [board, setBoard] = useState(createBoard(level.size));
	const cells = board.flat();

	return (
		<table>
			{
				board.map((row, y) =>
					<tr key={y}>
						{
							row.map((cell, x) => {
								const getAdjacent = ({ pos }: Cell) =>
									range(3, dx => range(3, dy => [pos[0] + dx - 1, pos[1] + dy - 1]))
										.flat()
										.map(([x, y]) => board[y]?.[x])
										.filter(Boolean);
								const openCells = (cellsToOpen: Cells) => {
									for (const cell of cellsToOpen) cell.open = true;
									setBoard([...board]);
								};
								const adjacent = getAdjacent(cell);
								return (
									<td
										key={x}
										style={{
											height: 44,
											width: 44
										}}
									>
										{
											cell.open
												? ``
												: <button
													className='btn btn-primary'
													style={{ width: `100%`, height: `100%` }}
													onClick={() => {
														if (cells.every(({ mine }) => !mine)) {
															const candidates = cells.filter(candidate =>
																!adjacent.includes(candidate)
															);
															range(
																mines,
																() =>
																	candidates.splice(
																		Math.floor(
																			Math.random() * candidates.length
																		),
																		1
																	)[0].mine = true
															);
														}
														openCells([cell]);
													}}
												/>
										}
									</td>
								);
							})
						}
					</tr>
				)
			}
		</table>
	);

};

export default App;